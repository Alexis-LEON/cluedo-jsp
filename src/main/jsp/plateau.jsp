<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cluedo</title>
</head>
<body>
	<h1>CLUEDO</h1>
	<div class="plateau">
		<table><% initializeTable(); %></table>
	</div>
</body>
<%!
	public void initializeTable() {
		int ligne = 25;
		int colonne = ligne;
		for(int i = 0; i < ligne; i++) {
			%> <tr> <%!
			for (int j = 0; j < colonne; j++) {
				%> <th></th> <%!
			}
			%> </tr> <%!
		}
	}
%>
</html>
